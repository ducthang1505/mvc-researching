package com.thang.mvc.demo.passive

import com.thang.mvc.demo.R
import com.thang.mvc.demo.model.entities.Task
import com.thang.mvc.demo.model.repositories.TaskRepository

class TaskController(
    private val passiveTaskRepository: TaskRepository,
    private val taskView: TaskActivity
) {

    init {
        taskView.refreshTasks()
    }

    suspend fun addNewTask(taskName: String) {
        val appendedTaskNameIndex = taskName +
                (passiveTaskRepository.getAllTaskFromCache().size + 1).toString()
        passiveTaskRepository.addNewTask(appendedTaskNameIndex)
        taskView.refreshTasks()
    }

    fun getTotalMemberCountText(tasks: List<Task>): String {
        val totalMember = tasks.sumBy { it.memberCount }
        return taskView.getString(R.string.total_members, totalMember)
    }
}