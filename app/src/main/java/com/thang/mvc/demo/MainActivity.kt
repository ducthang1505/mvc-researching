package com.thang.mvc.demo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thang.mvc.demo.active.StoreActivity
import kotlinx.android.synthetic.main.activity_main.*
import com.thang.mvc.demo.basic.TaskActivity as SimpleTaskActivity
import com.thang.mvc.demo.passive.TaskActivity as PassiveTaskActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnBasicMVC.setOnClickListener {
            startActivity(Intent(this, SimpleTaskActivity::class.java))
        }

        btnPassiveMVC.setOnClickListener {
            startActivity(Intent(this, PassiveTaskActivity::class.java))
        }

        btnActiveMVC.setOnClickListener {
            startActivity(Intent(this, StoreActivity::class.java))
        }
    }
}
