package com.thang.mvc.demo.active

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.thang.mvc.demo.R
import com.thang.mvc.demo.model.entities.StoreItem
import com.thang.mvc.demo.model.repositories.StoreRepository
import com.thang.mvc.demo.utils.roundOffDecimal
import kotlinx.android.synthetic.main.activity_store.*
import kotlinx.android.synthetic.main.layout_cart.*
import kotlinx.coroutines.launch

class StoreActivity : AppCompatActivity() {

    private lateinit var storeAdapter: StoreAdapter
    private lateinit var storeRepository: StoreRepository
    private lateinit var storeController: StoreController
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var itemSelectedAdapter: ItemSelectedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store)

        storeRepository = StoreRepository.getInstance()
        storeController = StoreController(storeRepository, this)

        bottomSheetBehavior = BottomSheetBehavior.from(layoutCart)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        storeAdapter = StoreAdapter(this) {
            storeController.addItemToCart(it)
        }
        itemSelectedAdapter = ItemSelectedAdapter(this,
            onAddClickListener = {
                storeController.addItemToCart(it)
            },
            onRemoveClickListener = {
                storeController.removeFromCart(it)
            })
        btnCheckout.setOnClickListener {
            lifecycleScope.launch {
                showHideProgress(true)
                storeController.buyItems()
                showHideProgress(false)
            }
        }
        layoutCartHeader.setOnClickListener {
            bottomSheetBehavior.state = when (bottomSheetBehavior.state) {
                BottomSheetBehavior.STATE_COLLAPSED -> BottomSheetBehavior.STATE_EXPANDED
                BottomSheetBehavior.STATE_EXPANDED -> BottomSheetBehavior.STATE_COLLAPSED
                else -> return@setOnClickListener
            }
        }
        setUpRvStore()
        setUpItemSelectedRecyclerView()
        refreshStoreItems()
        subscribeProfit()
    }

    private fun refreshStoreItems() {
        lifecycleScope.launch {
            showHideProgress(true)
            val storeItems = storeRepository.getAllStoreItems()
            storeAdapter.submitList(storeItems)
            storeAdapter.notifyDataSetChanged()
            showHideProgress(false)
        }
    }

    private fun setUpRvStore() {
        rvListStoreItem.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(this, 2)
        rvListStoreItem.layoutManager = layoutManager
        rvListStoreItem.adapter = storeAdapter
    }

    private fun setUpItemSelectedRecyclerView() {
        rvItemSelected.setHasFixedSize(true)
        rvItemSelected.adapter = itemSelectedAdapter
    }

    private fun subscribeProfit() {
        storeRepository.getProfitObservable().observe(this) {
            txtProfit.text = getString(R.string.profit, it.roundOffDecimal())
        }
    }

    private fun showHideProgress(show: Boolean) {
        progressLoading.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun refreshItemsSelected(dataDisplay: List<Pair<StoreItem, Int>>) {
        itemSelectedAdapter.submitList(dataDisplay)
        itemSelectedAdapter.notifyDataSetChanged()
    }

    fun showHideLayoutCard(show: Boolean) {
        bottomSheetBehavior.state = if (show) {
            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) return
            BottomSheetBehavior.STATE_COLLAPSED
        } else BottomSheetBehavior.STATE_HIDDEN
    }

    fun updateItemSelectedCount(sum: Int) {
        tvItemCount.text = sum.toString()
    }

    @SuppressLint("SetTextI18n")
    fun updateTotalPrice(total: Double) {
        tvTotalPrice.text = total.roundOffDecimal() + "$"
    }
}
