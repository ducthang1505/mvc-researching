package com.thang.mvc.demo.active

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.thang.mvc.demo.R
import com.thang.mvc.demo.model.entities.StoreItem
import kotlinx.android.synthetic.main.item_store_selected.view.*

class ItemSelectedAdapter(
    private val context: Context,
    private val onAddClickListener: (StoreItem) -> Unit,
    private val onRemoveClickListener: (StoreItem) -> Unit
) : ListAdapter<Pair<StoreItem, Int>, ItemSelectedViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemSelectedViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_selected, parent, false)
        return ItemSelectedViewHolder(view, onAddClickListener, onRemoveClickListener)
    }

    override fun onBindViewHolder(
        holder: ItemSelectedViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {

        val (storeItem, count) = getItem(position)
        var fullRefresh = payloads.isEmpty()
        if (payloads.isNotEmpty()) {
            payloads.forEach { payload ->
                when (payload) {
                    1 -> holder.itemView.tvItemCount.text = count.toString()
                    else -> fullRefresh = true
                }
            }
        }
        // Normally we only fully refresh the list item if it's being initially bound, but
        // we might also do it if there was a payload that wasn't understood, just to ensure
        // there isn't a stale item.
        if (fullRefresh) {
            holder.itemView.tvItemCount.text = count.toString()
            holder.itemView.tvName.text = storeItem.name
            Glide.with(context).load(storeItem.image).into(holder.itemView.ivStoreItem)
        }
        holder.item = storeItem
    }

    override fun onBindViewHolder(holder: ItemSelectedViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }


}

class ItemSelectedViewHolder(
    view: View,
    onAddClickListener: (StoreItem) -> Unit,
    onRemoveClickListener: (StoreItem) -> Unit
) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    var item: StoreItem? = null

    init {
        view.btnAdd.setOnClickListener {
            item?.let { onAddClickListener(it) }
        }
        view.btnRemove.setOnClickListener {
            item?.let { onRemoveClickListener(it) }
        }
    }
}

val diffCallback = object : DiffUtil.ItemCallback<Pair<StoreItem, Int>>() {
    override fun areItemsTheSame(
        oldItem: Pair<StoreItem, Int>,
        newItem: Pair<StoreItem, Int>
    ): Boolean =
        StoreItem.diffCallback.areItemsTheSame(oldItem.first, newItem.first)

    override fun areContentsTheSame(oldItem: Pair<StoreItem, Int>, newItem: Pair<StoreItem, Int>) =
        oldItem.first == newItem.first && oldItem.second == newItem.second


    override fun getChangePayload(oldItem: Pair<StoreItem, Int>, newItem: Pair<StoreItem, Int>) =
        if (oldItem.second != newItem.second) 1 else null

}