package com.thang.mvc.demo.active

import com.thang.mvc.demo.model.entities.StoreItem
import com.thang.mvc.demo.model.repositories.StoreRepository

class StoreController(
    private val storeRepository: StoreRepository,
    private val storeView: StoreActivity
) {

    private val itemSelected = mutableMapOf<StoreItem, Int>()

    fun addItemToCart(storeItem: StoreItem) {
        val count = itemSelected[storeItem] ?: 0
        itemSelected[storeItem] = count + 1
        notifyViewUpdateItemsSelected()
    }

    fun removeFromCart(storeItem: StoreItem) {
        if (!itemSelected.containsKey(storeItem)) return
        var count = itemSelected[storeItem] ?: return
        count--
        if (count <= 0) {
            itemSelected.remove(storeItem)
        } else {
            itemSelected[storeItem] = count
        }
        notifyViewUpdateItemsSelected()
    }

    suspend fun buyItems() {
        storeRepository.buyItems(itemSelected)
        itemSelected.clear()
        notifyViewUpdateItemsSelected()
    }

    private fun notifyViewUpdateItemsSelected() {
        var totalPrice = 0.0
        val dataDisplay = mutableListOf<Pair<StoreItem, Int>>()
        itemSelected.forEach { (storeItem, count) ->
            dataDisplay.add(Pair(storeItem, count))
            totalPrice += storeItem.price * count
        }
        storeView.refreshItemsSelected(dataDisplay)
        storeView.updateItemSelectedCount(itemSelected.values.sum())
        storeView.showHideLayoutCard(dataDisplay.isNotEmpty())
        storeView.updateTotalPrice(totalPrice)
    }


}