package com.thang.mvc.demo.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.thang.mvc.demo.R
import com.thang.mvc.demo.model.entities.Task
import kotlinx.android.synthetic.main.item_task.view.*

open class TaskAdapter(private val itemClickedListener: (Task) -> Unit) :
    ListAdapter<Task, TaskViewHolder>(Task.diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_task, parent, false)
        return TaskViewHolder(view, itemClickedListener)
    }

    override fun onBindViewHolder(
        holder: TaskViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {

        val task = getItem(position)
        val fullRefresh = payloads.isEmpty()


        // Normally we only fully refresh the list item if it's being initially bound, but
        // we might also do it if there was a payload that wasn't understood, just to ensure
        // there isn't a stale item.
        if (fullRefresh) {
            holder.itemView.numberOfTask.text = (position + 1).toString()
            holder.itemView.taskName.text = task.name
            holder.itemView.taskStatus.text = task.status.name
        }
        holder.item = task
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }
}

class TaskViewHolder(
    view: View,
    itemClickedListener: (Task) -> Unit
) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    var item: Task? = null

    init {
        view.setOnClickListener {
            item?.let { itemClickedListener(it) }
        }
    }
}