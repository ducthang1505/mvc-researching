package com.thang.mvc.demo.active

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.thang.mvc.demo.R
import com.thang.mvc.demo.model.entities.StoreItem
import kotlinx.android.synthetic.main.item_store.view.*

class StoreAdapter(
    private val context: Context,
    private val itemClickedListener: (StoreItem) -> Unit
) : ListAdapter<StoreItem, StoreViewHolder>(StoreItem.diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store, parent, false)
        return StoreViewHolder(view, itemClickedListener)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(
        holder: StoreViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {

        val storeItem = getItem(position)
        val fullRefresh = payloads.isEmpty()


        // Normally we only fully refresh the list item if it's being initially bound, but
        // we might also do it if there was a payload that wasn't understood, just to ensure
        // there isn't a stale item.
        if (fullRefresh) {
            holder.itemView.tvPrice.text = storeItem.price.toString() + "$"
            holder.itemView.tvName.text = storeItem.name
            Glide.with(context).load(storeItem.image).into(holder.itemView.ivStoreItem)
        }
        holder.item = storeItem
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }
}

class StoreViewHolder(
    view: View,
    itemClickedListener: (StoreItem) -> Unit
) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    var item: StoreItem? = null

    init {
        view.btnAdd.setOnClickListener {
            item?.let { itemClickedListener(it) }
        }
    }
}