package com.thang.mvc.demo.model.types

enum class TaskStatus {
    TODO,
    IN_PROGRESS,
    DONE
}