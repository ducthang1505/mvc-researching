package com.thang.mvc.demo.model.types

enum class StoreItemType(val profitPercent: Int) {
    WINE(6),
    SNACK(2),
    CAKE(3)
}