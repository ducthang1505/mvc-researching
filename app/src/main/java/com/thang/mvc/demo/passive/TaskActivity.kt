package com.thang.mvc.demo.passive

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.thang.mvc.demo.R
import com.thang.mvc.demo.base.TaskAdapter
import com.thang.mvc.demo.model.repositories.TaskRepository
import kotlinx.android.synthetic.main.activity_list_task.*
import kotlinx.coroutines.launch

class TaskActivity : AppCompatActivity() {

    private lateinit var taskController: TaskController
    private lateinit var passiveTaskRepository: TaskRepository
    private val taskAdapter = TaskAdapter {
        Toast.makeText(this, it.name, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_task)

        passiveTaskRepository = TaskRepository.getInstance()
        taskController = TaskController(passiveTaskRepository, this)

        setUpListTasksRecyclerView()
        fabAddTask.setOnClickListener {
            lifecycleScope.launch {
                taskController.addNewTask("abc")
            }
        }
    }

    fun refreshTasks() {
        lifecycleScope.launch {
            showHideProgress(true)
            val allTask = passiveTaskRepository.getAllTask()
            taskAdapter.submitList(allTask)
            taskAdapter.notifyDataSetChanged()
            txtTotalMember.text = taskController.getTotalMemberCountText(allTask)
            showHideProgress(false)
        }
    }

    private fun setUpListTasksRecyclerView() {
        rvListTask.setHasFixedSize(true)
        rvListTask.adapter = taskAdapter
    }

    private fun showHideProgress(show: Boolean) {
        progressLoading.visibility = if (show) View.VISIBLE else View.GONE
    }
}
