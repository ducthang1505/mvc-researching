package com.thang.mvc.demo.model.repositories

import com.thang.mvc.demo.model.entities.Task
import com.thang.mvc.demo.model.types.TaskStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.util.*

/**
 * This class is Model in MVC World
 */
class TaskRepository private constructor() {

    companion object {
        private val passiveTaskRepository = TaskRepository()

        fun getInstance(): TaskRepository = passiveTaskRepository

    }

    private val listOfTasks = mutableListOf(
        Task(UUID.randomUUID().toString(), "Researching MVC", TaskStatus.DONE, 2),
        Task(UUID.randomUUID().toString(), "Resolving bank issues", TaskStatus.TODO, 1),
        Task(UUID.randomUUID().toString(), "Learning Kotlin", TaskStatus.IN_PROGRESS, 1)
    )

    suspend fun getAllTask(): List<Task> = withContext(Dispatchers.IO) {
        //Make delay
        delay(1000)
        return@withContext listOfTasks
    }

    suspend fun addNewTask(taskName: String) = withContext(Dispatchers.Default) {
        delay(200)
        listOfTasks.add(
            Task(
                UUID.randomUUID().toString(),
                taskName,
                TaskStatus.TODO,
                Random().nextInt(10)
            )
        )
    }

    fun getAllTaskFromCache(): List<Task> = listOfTasks

}