package com.thang.mvc.demo.model.entities

import androidx.recyclerview.widget.DiffUtil
import com.thang.mvc.demo.model.types.StoreItemType

data class StoreItem(
    val id: String,
    val name: String,
    val type: StoreItemType,
    val price: Double,
    var remaining: Int,
    val image: String
) {
    companion object {
        /**
         * [DiffUtil.ItemCallback] for a [StoreItem].
         *
         * Since all [StoreItem]s have a unique ID, it's easiest to check if two
         * items are the same by simply comparing that ID.
         *
         * To check if the contents are the same, we use the same ID, but it may be the
         * case that it's only the play state itself which has changed (from playing to
         * paused, or perhaps a different item is the active item now). In this case
         * we check both the ID and the playback resource.
         */
        val diffCallback = object : DiffUtil.ItemCallback<StoreItem>() {
            override fun areItemsTheSame(
                oldItem: StoreItem,
                newItem: StoreItem
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: StoreItem, newItem: StoreItem) =
                oldItem == newItem

        }
    }
}