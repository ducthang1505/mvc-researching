package com.thang.mvc.demo.model.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.thang.mvc.demo.model.entities.StoreItem
import com.thang.mvc.demo.model.types.StoreItemType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import java.util.*

class StoreRepository private constructor() {

    companion object {
        private val storeRepository = StoreRepository()

        fun getInstance() = storeRepository
    }

    private var profitPublisher = MutableLiveData<Double>(0.0)

    private val storeItems = listOf(
        StoreItem(
            UUID.randomUUID().toString(),
            "Chivas",
            StoreItemType.WINE,
            100.0,
            10,
            "https://winehousenigeria.com/wp-content/uploads/2018/08/blend_chi2.jpg"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "Donut",
            StoreItemType.CAKE,
            1.2,
            100,
            "https://i1.wp.com/congthucmonngon.com/wp-content/uploads/2018/04/cach-lam-banh-donut-dau-tay-ngon-kho-cuong.jpg?ssl=1"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "Poca",
            StoreItemType.SNACK,
            0.6,
            200,
            "https://cf.shopee.vn/file/459d025ec630b5f8d0cdba5e82e7506e"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "Hennessy X.0",
            StoreItemType.WINE,
            120.0,
            6,
            "https://sanhruou.com/media/image/1078/ruou-hennessy-xo.jpg"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "O'Star",
            StoreItemType.SNACK,
            0.7,
            300,
            "https://cf.shopee.vn/file/459d025ec630b5f8d0cdba5e82e7506e"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "Cosy",
            StoreItemType.CAKE,
            1.5,
            43,
            "https://khonguyenlieu.com/wp-content/uploads/2017/12/cosy1.png"
        ),
        StoreItem(
            UUID.randomUUID().toString(),
            "Heinz",
            StoreItemType.SNACK,
            0.7,
            150,
            "https://images-na.ssl-images-amazon.com/images/I/81tIfFLXSyL._AC_SL1500_.jpg"
        )

    )

    suspend fun buyItems(storeItems: Map<StoreItem, Int>) = withContext(Dispatchers.IO) {
        delay(300)
        var profit = profitPublisher.value ?: 0.0
        storeItems.forEach {
            it.key.remaining -= it.value
            profit += it.value * it.key.price * (it.key.type.profitPercent / 100.0)
        }

        profitPublisher.postValue(profit)
    }

    fun getProfitObservable(): LiveData<Double> = profitPublisher

    suspend fun getAllStoreItems() = withContext(Dispatchers.IO) {
        delay(500)
        return@withContext storeItems
    }

}