package com.thang.mvc.demo.model.entities

import androidx.recyclerview.widget.DiffUtil
import com.thang.mvc.demo.model.types.TaskStatus


data class Task(
    val id: String,
    val name: String,
    val status: TaskStatus,
    val memberCount: Int = 0
) {


    companion object {
        /**
         * [DiffUtil.ItemCallback] for a [Task].
         *
         * Since all [Task]s have a unique ID, it's easiest to check if two
         * items are the same by simply comparing that ID.
         *
         * To check if the contents are the same, we use the same ID, but it may be the
         * case that it's only the play state itself which has changed (from playing to
         * paused, or perhaps a different item is the active item now). In this case
         * we check both the ID and the playback resource.
         */
        val diffCallback = object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(
                oldItem: Task,
                newItem: Task
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Task, newItem: Task) =
                oldItem == newItem

        }
    }
}