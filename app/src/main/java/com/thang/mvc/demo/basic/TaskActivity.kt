package com.thang.mvc.demo.basic

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.thang.mvc.demo.R
import com.thang.mvc.demo.base.TaskAdapter
import com.thang.mvc.demo.model.repositories.TaskRepository
import kotlinx.android.synthetic.main.activity_list_task.*
import kotlinx.coroutines.launch

/**
 * In Basic MVC
 * This activity is controller
 */
class TaskActivity : AppCompatActivity() {

    private lateinit var taskRepository: TaskRepository

    private val taskAdapter = TaskAdapter {
        Toast.makeText(this, it.name, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_task)
        taskRepository = TaskRepository.getInstance()

        fabAddTask.setOnClickListener { addNewTask() }
        setUpListTasksRecyclerView()

        getAllTask()
    }

    private fun addNewTask() {
        showHideProgress(true)
        lifecycleScope.launch {
            taskRepository.addNewTask("New Task ${taskAdapter.itemCount + 1}")
            getAllTask()
        }
    }

    private fun getAllTask() {
        showHideProgress(true)
        lifecycleScope.launch {
            val allTask = taskRepository.getAllTask()
            taskAdapter.submitList(allTask)
            //UI Logic: Calculate total member
            txtTotalMember.text =
                getString(R.string.total_members, allTask.sumBy { it.memberCount })
            taskAdapter.notifyDataSetChanged()
            showHideProgress(false)
        }
    }

    private fun setUpListTasksRecyclerView() {
        rvListTask.setHasFixedSize(true)
        rvListTask.adapter = taskAdapter
    }

    private fun showHideProgress(show: Boolean) {
        progressLoading.visibility = if (show) View.VISIBLE else View.GONE
    }
}
